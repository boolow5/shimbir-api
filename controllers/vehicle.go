package controllers

import (
	"net/http"
	"strings"

	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// GetVehicleTypes returns all supported vehicle type
func GetVehicleTypes(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"vehicle_types": models.VehicleTypes})
}

// ReloadVehicleTypes updates vehicle types for json file
func ReloadVehicleTypes(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.AgentUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "agent level or higher is required"))
		return
	}
	count, err := models.LoadVehicleTypes()
	if err != nil {
		logger.Errorf("failed to reload vehicle types: %v", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success", "count": count})
}

// RegisterVehicle creates new vehicle
func RegisterVehicle(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.AgentUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "only agent can register a vehicle"))
		return
	}
	input := struct {
		Type             string          `json:"type"`
		Model            string          `json:"model"`
		Manufacturer     string          `json:"manufacturer"`
		Seats            int             `json:"seats"`
		NumberPlate      string          `json:"number"`
		HasAC            bool            `json:"has_ac"`
		DisabilityAccess bool            `json:"disability_access"`
		OwnerID          bson.ObjectId   `json:"owner_id" `
		Drivers          []bson.ObjectId `json:"drivers"`
	}{}

	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	if !models.IsValidVehicleType(input.Type) {
		logger.Error("invalid vehicle type: ", input.Type)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid vehicle type"))
		return
	}
	if input.Seats < 1 {
		input.Seats = 1
	}
	if len(strings.TrimSpace(input.NumberPlate)) < 4 {
		logger.Error("invalid vehicle number plate: ", input.Type)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid vehicle number plate"))
		return
	}
	if len(input.OwnerID.Hex()) != 24 {
		allValid := len(input.Drivers) > 0
		for i := 0; i < len(input.Drivers); i++ {
			if len(input.Drivers[i].Hex()) != 24 {
				allValid = false
				break
			}
		}
		if !allValid {
			logger.Error("invalid vehicle/driver owner id: ", input.Type)
			AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid vehicle owner/driver id"))
			return
		}
	}

	// TODO: add picture uploading feature
	vehicle := models.NewVehicle(input.Type, input.Model, input.Manufacturer, input.NumberPlate, "", input.Seats, input.HasAC, input.DisabilityAccess, input.OwnerID, input.Drivers)

	errs := models.Save(&vehicle)
	if len(errs) > 0 {
		logger.Error("binding form failed: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "success", "vehicle": vehicle})
}

// GetVehicles returns user's vehicles
func GetVehicles(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.DriverUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "driver or higher level is required"))
		return
	}
	ownVehicles := c.Query("own") == "true"
	driveVehicles := c.Query("drive") == "true"
	vehicles, err := user.GetVehicles(ownVehicles, driveVehicles)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"vehicles": vehicles})
}
