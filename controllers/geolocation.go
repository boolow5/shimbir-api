package controllers

import (
	"net/http"
	"strings"

	"github.com/codingsince1985/geo-golang"

	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
)

// ReverseGeocode takes coordinates and returns address
func ReverseGeocode(c *gin.Context) {
	input := struct {
		Lat  float64 `json:"lat"`
		Long float64 `json:"long"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Binding error: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	location := models.Location{Lat: input.Lat, Long: input.Long}
	err = location.ReverseGeocode()
	if err != nil && strings.Contains(err.Error(), "invalid address returned") {
		logger.Errorf("Failed to reverse geocode: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"location": location})
}

// Geocode takes an address and returns coordinates
func Geocode(c *gin.Context) {
	input := struct {
		Address string `json:"address"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Binding error: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	location := models.Location{Address: geo.Address{FormattedAddress: input.Address}}
	err = location.Geocode()
	if err != nil && !strings.Contains(err.Error(), "invalid location response") {
		logger.Errorf("Failed to geocode: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	} else if strings.Contains(err.Error(), "invalid location response") {

	}
	c.JSON(http.StatusOK, gin.H{"location": location})
}

// CalcuateDistance calculates distance (meters) between two points on earth
func CalcuateDistance(c *gin.Context) {
	input := struct {
		From models.Location `json:"from"`
		To   models.Location `json:"to"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Failed to bind location data: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	distance := models.GetDistance(input.From, input.To)
	c.JSON(http.StatusOK, gin.H{
		"distance": distance,
		"unit":     "meter",
		// "from":     []float64{input.From.Lat, input.From.Long},
		// "to":       []float64{input.From.Lat, input.From.Long},
	})
}

// GetNearbyDistricts gets districts close to the passed coordinates
func GetNearbyDistricts(c *gin.Context) {
	input := struct {
		Lat    float64 `json:"lat"`
		Long   float64 `json:"long"`
		Radius float64 `json:"radius"`
	}{}
	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Failed to bind coordinates data: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	results, err := models.GetNearbyDistricts(input.Lat, input.Long, input.Radius)
	if err != nil {
		logger.Errorf("Failed to get nearby districts: %v ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"request": input,
		"results": results,
	})
}
