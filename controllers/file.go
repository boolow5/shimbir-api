package controllers

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// UploadPicture uplaods a picture to server
func UploadPicture(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}

	filePurpose := models.StaticFilePurposeProfilePic
	itemID := userID
	vehicleIDStr := c.Query("vehicleID")
	if len(vehicleIDStr) == 24 && user.Level < models.AgentUserLevel {
		logger.Error("permission error: vehicle picture can be updated only by agent")
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "permission denied"))
		return
	} else if len(vehicleIDStr) == 24 {
		itemID = bson.ObjectIdHex(vehicleIDStr)
		filePurpose = models.StaticFilePurposeVehiclePic
	}

	driverIDStr := c.Query("driverID")
	if len(driverIDStr) == 24 && user.Level < models.AgentUserLevel {
		logger.Error("permission error: driver picture can be updated only by agent")
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "permission denied"))
		return
	} else if len(driverIDStr) == 24 {
		itemID = bson.ObjectIdHex(driverIDStr)
		filePurpose = models.StaticFilePurposeDriverPic
	}

	targetItem := ""

	var item models.DBObject
	switch filePurpose {
	case models.StaticFilePurposeProfilePic:
		item = &models.User{ID: itemID}
		err = models.GetItemByID(itemID, item)
		targetItem = "user"
	case models.StaticFilePurposeVehiclePic:
		item = &models.Vehicle{ID: itemID}
		err = models.GetItemByID(itemID, item)
		targetItem = "vehicle"
	case models.StaticFilePurposeDriverPic:
		item = &models.User{ID: itemID}
		err = models.GetItemByID(itemID, item)
		targetItem = "driver"
	}

	if err != nil {
		logger.Error("finding target item failed: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, []error{fmt.Errorf("cannot find %s by id", targetItem), err}))
		return
	}

	staticDirName := config.Get().StaticDirectory

	staticPath := config.GetStaticDirectory()

	if err != nil {
		logger.Error("creating directory failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	dir := filepath.Join(staticPath, userID.Hex())

	err = os.MkdirAll(dir, 0722)
	if err != nil && os.IsExist(err) {
		// skip
	} else if err != nil {
		logger.Error("creating directory failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	header, err := c.FormFile("picture")
	if err != nil {
		logger.Error("uploading failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	fmt.Printf("\nHEADER: %+v\n", header.Header)
	fileName := fmt.Sprintf("%s-%s", time.Now().Format("2006-01-02-15_04_05"), header.Filename)
	fileName = strings.Replace(fileName, " ", "_", -1)
	fileName = strings.Replace(fileName, "#", "", -1)
	file := models.StaticFile{
		Name:    fileName,
		Dir:     dir,
		URL:     filepath.Join(staticDirName, userID.Hex(), fileName),
		UserID:  userID,
		ItemID:  itemID,
		UsedFor: filePurpose,
	}
	fullPath := filepath.Join(dir, fileName)
	err = c.SaveUploadedFile(header, fullPath)
	if err != nil {
		logger.Error("saving uploaded file failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(SavingItemError, err.Error()))
		return
	}
	// find the old file
	oldFiles, err := models.GetStaticFiles(userID, item.GetID())
	if err != nil {
		logger.Errorf("finding old files failed: %v", err)
	}

	errs := models.Save(&file)
	if len(errs) > 0 {
		logger.Error("saving file data failed: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	switch targetItem {
	case "user":
		user := item.(*models.User)
		user.Profile.ProfilePic = file.URL
		errs = models.Update(user)
	case "vehicle":
		vehicle := item.(*models.Vehicle)
		vehicle.PictureURL = file.URL
		errs = models.Update(vehicle)
	case "driver":
		driver := item.(*models.User)
		driver.Profile.ProfilePic = file.URL
		errs = models.Update(driver)
	}
	if len(errs) > 0 {
		logger.Errorf("updating %s failed: %v", targetItem, errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(SavingItemError, errs))
		return
	}
	fmt.Println("old files:", len(oldFiles))
	// delete the old file
	for i := 0; i < len(oldFiles); i++ {
		// remove the file from filesystem
		err = os.Remove(oldFiles[i].URL)
		if err != nil {
			fmt.Printf("%d. Failed to delete file, ERROR: %v\n", i, err)
		}
		// remove file data from database
		err = models.Remove(&oldFiles[i])
		if err != nil {
			fmt.Printf("%d. OLD FILE: %+v\nERROR: %v\n", i, oldFiles[i], err)
		}
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success", "file": file})
}
