package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/notifications"
	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"

	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddDriver adds new driver, only accessible to AgentUserLevel, ManagerUserLevel, AdminUserLevel
func AddDriver(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.AgentUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := struct {
		Email         string    `json:"email"`
		Phone         string    `json:"phone"`
		Password      string    `json:"password"`
		FirstName     string    `json:"first_name"`
		MiddleName    string    `json:"middle_name"`
		LastName      string    `json:"last_name"`
		Birthday      time.Time `json:"birthday"`
		ProfilePic    string    `json:"profile_pic"`
		LicenseNumber string    `json:"licence_number"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding error: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	// check if user already exists
	// check with phone first
	var adminUser models.User
	var foundBy string
	var changes []string
	var errs []error
	adminUser, err = models.GetUserByPhone(input.Phone)
	if input.Phone != "" && err == nil {
		foundBy = "phone"
	}
	if foundBy == "" && input.Email != "" {
		adminUser, err = models.GetUserByEmail(input.Email)
	}
	if err == nil {
		foundBy = "email"
	}
	alreadyExists := !db.IsNotFound(err)
	logger.Errorf("Already exists? %v, ERROR: %v", alreadyExists, err)
	if alreadyExists {
		if adminUser.Level < models.DriverUserLevel {
			changes = append(changes, string("level"))
			adminUser.Level = models.DriverUserLevel
		}
		if !utils.IsValidEmail(adminUser.Email) && adminUser.Email != input.Email && input.Email != "" {
			changes = append(changes, string("email"))
			adminUser.Email = input.Email
		} else if input.Phone != "" && adminUser.Phone != input.Phone {
			changes = append(changes, string("phone"))
			adminUser.Phone = input.Phone
		}
		if input.FirstName != "" {
			changes = append(changes, string("first name"))
			adminUser.Profile.FirstName = input.FirstName
		}
		if input.MiddleName != "" {
			changes = append(changes, string("middle name"))
			adminUser.Profile.MiddleName = input.MiddleName
		}
		if input.LastName != "" {
			changes = append(changes, string("last name"))
			adminUser.Profile.LastName = input.LastName
		}
		if !input.Birthday.IsZero() {
			changes = append(changes, string("birthday"))
			adminUser.Profile.Birthday = input.Birthday
		}
		if input.ProfilePic != "" {
			changes = append(changes, string("profile picture"))
			adminUser.Profile.ProfilePic = input.ProfilePic
		}
		if len(adminUser.Profile.UserID.Hex()) != 24 {
			adminUser.Profile.UserID = adminUser.ID
		}
		if len(input.LicenseNumber) > 3 {
			adminUser.LicenseNumber = input.LicenseNumber
		}
		logger.Debugf("Profile id: %v === %v", adminUser.Profile.UserID, adminUser.ID)
		errs = models.Update(&adminUser)
	} else {
		if len(input.LicenseNumber) < 4 {
			logger.Error("invalid license number: ", err)
			AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "Cannot register driver without license"))
			return
		}
		changes = append(changes, string("all"))
		adminUser = models.User{
			Email:         input.Email,
			Phone:         input.Phone,
			Password:      input.Password,
			Level:         models.DriverUserLevel,
			LicenseNumber: input.LicenseNumber,
			Profile: models.Profile{
				FirstName:  input.FirstName,
				MiddleName: input.MiddleName,
				LastName:   input.LastName,
				Birthday:   input.Birthday,
				ProfilePic: input.ProfilePic,
			},
		}
		errs = models.Save(&adminUser)
	}
	// check with email second

	if errs != nil && len(errs) > 0 {
		if alreadyExists {
			logger.Error("failed to update driver: ", errs)
		} else {
			logger.Error("failed to save driver: ", errs)
		}
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	err = adminUser.InitRatings()
	if err != nil {
		logger.Errorf("Failed to initialize ratings: %v", err)
	}
	if utils.IsValidEmail(adminUser.Email) {
		go func() {
			subject := fmt.Sprintf("You are verified driver")
			body := "You have successfully got a  to driver.\nYou will be able to accept passengers.\nCredentials:"

			response := notifications.NewMailResponse(
				subject,
				body,
				"",
				"Thanks for using our service",
				map[string]string{"Email": adminUser.Email, "Phone number": adminUser.Phone, "Password": input.Password},
				notifications.MsgTypeReceivedPayment,
			)
			email := notifications.NewEmail(subject, notifications.InfoEmail, response, adminUser.Email)
			resp := email.Send()
			if resp.Error != nil {
				fmt.Printf("EMAIL ERROR: %v\n", resp.Error)
			}
		}()
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success", "found_by": foundBy, "changes": changes})
}

// UpdateDriver changes driver data in the db, only accessible to AgentUserLevel, ManagerUserLevel, AdminUserLevel
func UpdateDriver(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.AgentUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	driverIDStr := c.Query("driverID")
	if len(driverIDStr) != 24 {
		logger.Errorf("invalid driver id: '%d'", driverIDStr)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	input := struct {
		Email      string    `json:"email"`
		Phone      string    `json:"phone"`
		Password   string    `json:"password"`
		FirstName  string    `json:"first_name"`
		MiddleName string    `json:"middle_name"`
		LastName   string    `json:"last_name"`
		Birthday   time.Time `json:"birthday"`
		ProfilePic string    `json:"profile_pic"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding error: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	driver := models.User{
		Email:    input.Email,
		Password: input.Password,
		Level:    models.DriverUserLevel,
		Profile: models.Profile{
			FirstName:  input.FirstName,
			MiddleName: input.MiddleName,
			LastName:   input.LastName,
			Birthday:   input.Birthday,
			ProfilePic: input.ProfilePic,
		},
	}
	errs := models.Save(&driver)
	if len(errs) > 0 {
		logger.Error("failed to save driver: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	c.JSON(http.StatusOK, successResponse)
}

// ReloadAdminWallet creates new balance, only accessible to admin
func ReloadAdminWallet(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	if user.Level < models.AdminUserLevel {
		logger.Errorf("user level very low: %d", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "permission denied"))
		return
	}
	wallets, err := user.GetWallets()
	if err != nil {
		logger.Error("finding user wallet failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	if len(wallets) < 1 {
		logger.Error("no wallets found: ", len(wallets))
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, "Found no wallets"))
		return
	}
	currencySymbol := c.Query("currencySymbol")
	amountStr := c.Query("amount")
	if currencySymbol == "" {
		currencySymbol = "$"
	}
	if amountStr == "" {
		amountStr = "0"
	}
	amount, err := strconv.ParseFloat(amountStr, 64)
	if err != nil {
		logger.Error("failed to parse amount from query: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	if amount == 0 {
		amount = 100
	}
	selectedWallet := wallets[0]
	for i := 0; i < len(wallets); i++ {
		if wallets[i].Currency.Symbol == currencySymbol && wallets[i].Name == models.AdminWallet {
			selectedWallet = wallets[i]
			break
		}
	}
	selectedWallet.Balance += amount
	errs := models.Update(&selectedWallet)
	if len(errs) > 0 {
		logger.Error("failed to update wallet: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	currency, _ := models.GetCurrencyBySymbol(currencySymbol)
	note := fmt.Sprintf("You reloaded %s %f. make sure you received the money!", currencySymbol, amount)
	trx := models.NewTransaction(userID, selectedWallet.ID, selectedWallet.ID, models.Money{Amount: amount, Currency: currency}, note)
	errs = models.Save(&trx)
	if len(errs) > 0 {
		logger.Error("failed to save reload transaction: ", err)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	go trx.InformSender()
	c.JSON(http.StatusOK, gin.H{"msg": note})
}
