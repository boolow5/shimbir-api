package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddTrip creates new trip order
func AddTrip(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	input := struct {
		From          models.Location `json:"from"`
		To            models.Location `json:"to"`
		VehicleType   string          `json:"vehicle_type"`
		Distance      float64         `json:"distance"`
		Price         float64         `json:"price"`
		Currency      string          `json:"currency"`
		ServiceAmount float64         `json:"service_amount"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	currency, err := models.GetCurrencyBySymbol(input.Currency)
	if err != nil {
		logger.Errorf("failed to find currency by symbol '%s': %v", input.Currency, err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	passenger := models.Passenger{
		UserID:   user.ID,
		FullName: fmt.Sprintf("%s %s %s", user.Profile.FirstName, user.Profile.MiddleName, user.Profile.LastName),
	}
	if len(user.ReviewsID.Hex()) == 24 {
		passenger.ReviewsID = user.ReviewsID
	}
	trip := models.Trip{
		CreatedBy:         userID,
		Start:             input.From,
		End:               input.To,
		Status:            models.TripStatusOrdered,
		EstimatedDistance: input.Distance,
		EstimatedPrice: models.Money{
			Amount:   input.Price,
			Currency: currency,
		},
		ServiceAmount: models.Money{
			Amount:   input.ServiceAmount,
			Currency: currency,
		},
		Passengers: []models.Passenger{
			passenger,
		},
	}
	errs := models.Save(&trip)
	if len(errs) > 0 {
		logger.Error("failed to save trip: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"trip": trip})
}

// UpdateTrip creates new trip order
func UpdateTrip(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	input := struct {
		TripID        bson.ObjectId   `json:"trip_id"`
		From          models.Location `json:"from"`
		To            models.Location `json:"to"`
		VehicleType   string          `json:"vehicle_type"`
		Distance      float64         `json:"distance"`
		Price         float64         `json:"price"`
		Currency      string          `json:"currency"`
		ServiceAmount float64         `json:"service_amount"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	currency, err := models.GetCurrencyBySymbol(input.Currency)
	if err != nil {
		logger.Errorf("failed to find currency by symbol '%s': %v", input.Currency, err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	passenger := models.Passenger{
		UserID:   user.ID,
		FullName: fmt.Sprintf("%s %s %s", user.Profile.FirstName, user.Profile.MiddleName, user.Profile.LastName),
	}
	if len(user.ReviewsID.Hex()) == 24 {
		passenger.ReviewsID = user.ReviewsID
	}
	trip := models.Trip{}
	err = models.GetItemByID(input.TripID, &trip)
	if err != nil {
		logger.Error("failed to find trip with id: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}

	trip.Start = input.From
	trip.End = input.To
	trip.Status = models.TripStatusOrdered
	trip.EstimatedDistance = input.Distance
	trip.EstimatedPrice = models.Money{
		Amount:   input.Price,
		Currency: currency,
	}
	trip.ServiceAmount = models.Money{
		Amount:   input.ServiceAmount,
		Currency: currency,
	}

	isInPassengers := false
	for i := 0; i < len(trip.Passengers); i++ {
		if trip.Passengers[i].UserID == userID {
			isInPassengers = true
			break
		}
	}

	if isInPassengers == false {
		trip.Passengers = append(trip.Passengers, passenger)
	}

	errs := models.Update(&trip)
	if len(errs) > 0 {
		logger.Error("failed to save trip: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(BadFormDataError, errs))
		return
	}
	c.JSON(http.StatusOK, gin.H{"trip": trip})
}

// CancelTrip is used for user to abort request for driver before driver accepts it
func CancelTrip(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	input := struct {
		TripID bson.ObjectId `json:"trip_id"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	trip := models.Trip{}
	err = models.GetItemByID(input.TripID, &trip)
	if err != nil {
		logger.Error("failed to find trip with id: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	if trip.CreatedBy != userID {
		logger.Error("permission denied this user is not passenger: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, err.Error()))
		return
	}
	trip.Status = models.TripStatusCancelled
	errs := models.Update(&trip)
	if len(errs) > 0 {
		logger.Error("failed to update trip: ", errs)
		AbortWithErrors(c, http.StatusBadRequest, NewErrors(PermissionError, errs))
		return
	}
	c.JSON(http.StatusOK, successResponse)
}

// AcceptTrip is used by drivers to accept trip request
func AcceptTrip(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	// check if user is driver
	if user.Level < models.DriverUserLevel {
		logger.Error("permission denied: user level ", user.Level)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "you don't have driver privilege"))
		return
	}

	input := struct {
		TripID          bson.ObjectId `json:"trip_id"`
		CarID           bson.ObjectId `json:"trip_car_id"`
		ResponseChannel string        `json:"response_channel"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	if len(input.ResponseChannel) < 5 {
		logger.Error("too short response channel: ", input.ResponseChannel)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid response channel"))
		return
	}

	if len(input.CarID.Hex()) != 24 {
		logger.Errorf("invalid car id", input.CarID.Hex())
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid vehicle id"))
		return
	}

	var vehicle models.Vehicle
	err = models.GetItemByID(input.CarID, &vehicle)
	if err != nil {
		logger.Error("car not found: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}

	var trip models.Trip

	err = models.GetItemByID(input.TripID, &trip)
	if err != nil {
		logger.Error("failed to find trip by id: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}

	wallet, err := user.GetWallet(trip.EstimatedPrice.Currency.Symbol)
	if err != nil {
		logger.Error("failed to find driver balance: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	if wallet.Balance < trip.ServiceAmount.Amount {
		logger.Errorf("balance '%.2f' is less than service amount '%.2f'", wallet.Balance, trip.ServiceAmount)
		AbortWithError(c, http.StatusBadRequest, NewError(PermissionError, "insufficient balance"))
		return
	}

	holdingWallet, err := models.GetHodlingWallet(trip.ServiceAmount.Currency.Symbol)
	if err != nil {
		logger.Error("failed to holding wallet: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, "internal error"))
		return
	}

	trx := models.NewTransaction(user.ID, wallet.ID, holdingWallet.ID, trip.ServiceAmount, fmt.Sprintf("Charged for accepting trip request"))
	err = trx.Complete()
	if err != nil {
		logger.Error("failed to complete transaction: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, "internal error"))
		return
	}
	trx.InformAll()

	err = trip.SetDriver(user, input.CarID, input.ResponseChannel, true)
	if err != nil {
		logger.Error("failed to set driver: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "Hurry up! passenger is waiting for you"})
}
