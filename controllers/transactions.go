package controllers

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// GetWallets is used to check current users wallets, it's good for checking balance
func GetWallets(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	wallets, err := user.GetWallets()
	if err != nil {
		logger.Error("finding user wallets failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, gin.H{"wallets": wallets})
}

// GetWallet finds wallet by its ID
func GetWallet(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	walletIDStr := c.Param("walletID")
	if len(walletIDStr) != 24 {
		logger.Error("invalid wallet id: ")
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, "invalid wallet id"))
		return
	}
	var walletID = bson.ObjectIdHex(walletIDStr)
	var wallet models.Wallet
	err = models.GetItemByID(walletID, &wallet)
	if err != nil {
		logger.Error("finding wallet by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}

	qrCodeFilePath, err := GetOrCreateQRCodeFile(wallet.GenerateURL(c.Request.Host), models.QRTypeWallet)
	if err != nil {
		logger.Error("finding wallet owner by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}

	if wallet.UserID == userID {
		c.JSON(http.StatusOK, gin.H{"wallet": wallet, "qrcode": qrCodeFilePath})
		return
	}

	wallet.Balance = 0
	var owner models.User
	err = models.GetItemByID(wallet.UserID, &owner)
	if err != nil {
		logger.Error("finding wallet owner by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"wallet": gin.H{
			"user_id": wallet.UserID,
			"name":    wallet.Name,
			"owner": gin.H{
				"full_name": owner.Profile.FullName(),
			},
		},
		"qrcode": qrCodeFilePath,
	})
}

// GetOrCreateQRCodeFile finds qrcode file if not found it creates new one
func GetOrCreateQRCodeFile(rawURL, qrType string) (fullPath string, err error) {
	fmt.Println("GetOrCreateQRCodeFile")
	qrc := models.NewQRCode(rawURL, qrType, models.QRResponse{})
	err = qrc.Verify()
	if err != nil {
		return "", err
	}
	qrcodeFile := qrc.GetQRCodeFileName()
	if qrcodeFile == "" {
		return "", fmt.Errorf("empty qrcode file name")
	}
	_, err = os.Open(qrcodeFile)
	if os.IsNotExist(err) {
		fmt.Println("Does not exist: ", os.IsNotExist(err), "\nERROR:", err)
		qrcodeFile, err = qrc.Generate()
		if err != nil {
			fmt.Println("Cannot Generate QR code file: ", os.IsNotExist(err), "\nERROR:", err)
			return qrcodeFile, err
		}
	}
	return qrcodeFile, nil
}

// TransferAmount is used for transfering from current user's balance to another user's balance
func TransferAmount(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	wallets, err := user.GetWallets()
	if err != nil {
		logger.Error("finding user wallets failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	if len(wallets) < 1 {
		logger.Errorf("%d wallets found: ", len(wallets))
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, "no wallets found"))
		return
	}
	input := struct {
		ReceiverID     bson.ObjectId `json:"receiver_id"`
		CurrencySymbol string        `json:"currency_symbol"`
		Amount         float64       `json:"amount"`
		Note           string        `json:"note"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	currency, err := models.GetCurrencyBySymbol(input.CurrencySymbol)
	if err != nil {
		logger.Error("finding currency by symbol failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	trx, err := models.TransferMoney(userID, input.ReceiverID, models.Money{Amount: input.Amount, Currency: currency}, input.Note)
	if err != nil {
		logger.Error("transfering money failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TransactionError, err.Error()))
		return
	}
	if trx.Step != models.TransactionStepAllSuccess {
		logger.Error("transfering state failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(TransactionError, "transaction is not successful"))
		return
	}
	c.JSON(http.StatusOK, gin.H{"transaction": trx, "msg": "success"})
}
