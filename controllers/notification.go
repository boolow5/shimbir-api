package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"bitbucket.org/boolow5/carpooling/shimbir-api/notifications"
	"github.com/gin-gonic/gin"
)

// Ping is used to test sse channel is working
func Ping(c *gin.Context) {
	channel := c.Query("channel")
	msg := []byte(fmt.Sprintf("[%s]\tPONG!", time.Now().Format(time.ANSIC)))
	targets := []string{"sse"}
	options := map[string]interface{}{
		"channel": channel,
	}
	err := notifications.Send(msg, targets, options)
	if err != nil {
		logger.Error("failed to send PONG message:", err)
		AbortWithError(c, http.StatusBadRequest, NewError(UknownError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, successResponse)
}

// NotifyDrivers sends notification to all nearby drivers to invite passenger who is expecting driver
func NotifyDrivers(c *gin.Context) {
	userID := bson.ObjectIdHex(c.MustGet("id").(string))
	user := models.User{}
	err := models.GetItemByID(userID, &user)
	if err != nil {
		logger.Error("finding user by id failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(AuthenticationError, err.Error()))
		return
	}
	input := struct {
		ID              bson.ObjectId `json:"trip_id"`
		NearbyDistricts []string      `json:"nearbyDistricts"`
		Price           float64       `json:"price"`
		Distance        float64       `json:"distance"`
		VehicleType     string        `json:"vehicle_type"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Error("binding form failed: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(BadFormDataError, err.Error()))
		return
	}
	trip := models.Trip{}
	err = models.GetItemByID(input.ID, &trip)
	if err != nil {
		logger.Error("failed to find trip with id: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	channels := []string{}
	for _, district := range input.NearbyDistricts {
		if district != "" {
			// channel is /trip/district/vehicle-type
			channels = append(channels, fmt.Sprintf("/trip/%s/%s", district, input.VehicleType))
		}
	}
	tempKey := bson.NewObjectId().Hex()
	responseChannel := fmt.Sprintf("/passenger/%s/%s", userID.Hex(), tempKey)
	msg := notifications.TripMessage{
		TripID:          input.ID,
		StartLocation:   &trip.Start,
		Price:           input.Price,
		CurrencySymbol:  trip.EstimatedPrice.Currency.Symbol,
		Distance:        input.Distance,
		VehicleType:     input.VehicleType,
		ResponseChannel: responseChannel,
	}
	data, err := json.Marshal(&msg)
	if err != nil {
		logger.Error("failed to marshal notification message: ", err)
		AbortWithError(c, http.StatusBadRequest, NewError(DataFetchingError, err.Error()))
		return
	}
	type failedChannel struct {
		Channel string `json:"channel"`
		Err     error  `json:"err"`
	}
	failedChannels := []failedChannel{}
	for _, channel := range channels {
		if channel != "" {
			err = notifications.Send(data, []string{"sse"}, map[string]interface{}{"channel": channel})
			if err != nil {
				failedChannels = append(failedChannels, failedChannel{Channel: channel, Err: err})
			}
		}
	}
	if len(channels) > len(failedChannels) && len(failedChannels) > 0 {
		c.JSON(http.StatusOK, gin.H{"responseChannel": responseChannel, "success_channels": len(channels) - len(failedChannels), "failed_channels": failedChannels})
	} else if len(failedChannels) == len(channels) {
		c.JSON(http.StatusInternalServerError, gin.H{"responseChannel": responseChannel, "msg": "all channels failed", "channel_count": len(channels)})
	} else {
		c.JSON(http.StatusOK, gin.H{"responseChannel": responseChannel, "success_channels": len(channels) - len(failedChannels)})
	}
}
