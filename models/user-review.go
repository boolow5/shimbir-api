package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// UserReview represents a review submitted by a user about another user
type UserReview struct {
	ID         bson.ObjectId `json:"id" bson:"_id"`
	ReviewerID bson.ObjectId `json:"reviewer_id" bson:"reviewer_id"`
	RevieweeID bson.ObjectId `json:"reviewee_id" bson:"reviewee_id"`
	Comment    string        `json:"comment" bson:"comment"`
	Rate       int           `json:"rate" bson:"rate"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (ur UserReview) GetColName() string {
	return UserReviewCollection
}

// GetID satisfies DBObject interface
func (ur UserReview) GetID() bson.ObjectId {
	return ur.ID
}

// SetID satisfies DBObject interface
func (ur *UserReview) SetID(id bson.ObjectId) {
	ur.ID = id
}

// SetTimestamp satisfies DBObject interface
func (ur *UserReview) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		ur.CreatedAt = now
		ur.UpdatedAt = now
	} else {
		ur.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (ur UserReview) IsValid() (valid bool, errs []error) {
	if len(ur.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid vehicle id")}...)
	}
	if len(ur.ReviewerID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid reviewer id")}...)
	}
	if len(ur.RevieweeID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid reviewee id")}...)
	}
	return true, errs
}
