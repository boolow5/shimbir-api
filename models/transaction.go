package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/notifications"
	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"

	"gopkg.in/mgo.v2/bson"
)

const (
	// TransactionStepInitiated means transaction was created but no transfer happened yet
	TransactionStepInitiated = iota
	// TransactionStepSenderFailed means transfer failed before sender was charged
	TransactionStepSenderFailed
	// TransactionStepSenderSuccess means transfer succeeded and sender was charged
	TransactionStepSenderSuccess
	// TransactionStepReceiverFailed means transfer failed before receiver is paid
	TransactionStepReceiverFailed
	// TransactionStepReceiverSuccess means transfer succeed and receiver was paid
	TransactionStepReceiverSuccess
	// TransactionStepAllSuccess means the transfer is 100% complete and saved successfully
	TransactionStepAllSuccess
)

// Transaction tracks a steps of transfer between two wallets
type Transaction struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	StartedBy      bson.ObjectId `json:"started_by" bson:"started_by"`
	SenderWallet   bson.ObjectId `json:"sender_wallet" bson:"sender_wallet"`
	ReceiverWallet bson.ObjectId `json:"receiver_wallet" bson:"receiver_wallet"`
	Amount         Money         `json:"amount" bson:"amount"`
	Note           string        `json:"note" bson:"note"`

	Step int `json:"step" bson:"step"`

	Timestamp `bson:",inline"`
}

// NewTransaction initializes transaction object
func NewTransaction(userID, fromWallet, toWallet bson.ObjectId, amount Money, note string) Transaction {
	return Transaction{
		StartedBy:      userID,
		SenderWallet:   fromWallet,
		ReceiverWallet: toWallet,
		Amount:         amount,
		Note:           note,
	}
}

// GetColName satisfies DBObject interface
func (t Transaction) GetColName() string {
	return TransactionCollection
}

// GetID satisfies DBObject interface
func (t Transaction) GetID() bson.ObjectId {
	return t.ID
}

// SetID satisfies DBObject interface
func (t *Transaction) SetID(id bson.ObjectId) {
	t.ID = id
}

// SetTimestamp satisfies DBObject interface
func (t *Transaction) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		t.CreatedAt = now
		t.UpdatedAt = now
	} else {
		t.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (t Transaction) IsValid() (valid bool, errs []error) {
	if len(t.StartedBy.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid transaction started user id")}...)
	}
	if len(t.SenderWallet.Hex()) != 24 {
		errs = append(errs, fmt.Errorf("invalid transaction sender wallet id"))
	}
	if len(t.ReceiverWallet.Hex()) != 24 {
		errs = append(errs, fmt.Errorf("invalid transaction receiver wallet id"))
	}
	if t.Amount.Amount <= 0 {
		errs = append(errs, fmt.Errorf("invalid transaction amount"))
	}
	if t.Amount.Currency.Symbol == "" {
		errs = append(errs, fmt.Errorf("invalid transaction currency"))
	}
	if len(t.ID.Hex()) != 24 {
		errs = append(errs, fmt.Errorf("invalid transaction id"))
	}
	return true, errs
}

// IsComplete checks if transaction is complete
func (t *Transaction) IsComplete() bool {
	return t.Step == TransactionStepAllSuccess
}

// Complete checks if transaction is complete and tries again to complete were it failed last time
func (t *Transaction) Complete() error {
	if t.Step == TransactionStepAllSuccess {
		return nil
	}
	switch t.Step {
	case TransactionStepInitiated, TransactionStepSenderFailed:
		// charge send and pay receiver
		senderWallet, err := GetWalletByCurrencySymbol(t.Amount.Currency.Symbol, t.StartedBy)
		if err != nil {
			return err
		}
		// check if sender has enough balance
		if senderWallet.Balance < t.Amount.Amount {
			return fmt.Errorf("insufficient balance")
		}
		err = senderWallet.Charge(t.Amount)
		if err != nil {
			return err
		}
		t.Step = TransactionStepSenderSuccess
		// pay receiver
		// get receiver wallet
		receiverWallet := Wallet{ID: t.ReceiverWallet}
		err = GetItemByFilter(WalletCollection, &bson.M{"_id": t.ReceiverWallet, "currency.symbol": t.Amount.Currency.Symbol}, &receiverWallet)
		if err != nil {
			return err
		}
		err = receiverWallet.Pay(t.Amount)
		if err != nil {
			t.Step = TransactionStepReceiverFailed
			return err
		}
		t.Step = TransactionStepReceiverSuccess
	case TransactionStepReceiverFailed:
		// pay receiver
		// get receiver wallet
		receiverWallet := Wallet{ID: t.ReceiverWallet}
		err := GetItemByFilter(WalletCollection, &bson.M{"_id": t.ReceiverWallet, "currency.symbol": t.Amount.Currency.Symbol}, &receiverWallet)
		if err != nil {
			return err
		}
		err = receiverWallet.Pay(t.Amount)
		if err != nil {
			t.Step = TransactionStepReceiverFailed
			return err
		}
		t.Step = TransactionStepReceiverSuccess
	}
	errs := Update(t)
	if len(errs) > 0 {
		return JoinErrors(errs)
	}
	t.InformAll()
	return nil
}

// TransferMoney processes transaction between two users
func TransferMoney(senderID, receiverID bson.ObjectId, money Money, note string) (*Transaction, error) {
	// get sender wallet
	senderWallet, err := GetWalletByCurrencySymbol(money.Currency.Symbol, senderID)
	if err != nil {
		return nil, err
	}
	// check if sender has enough balance
	if senderWallet.Balance < money.Amount {
		fmt.Printf("User [%s] wallet [%s] balance %.4f\tamount needed %.4f", senderWallet.UserID.Hex(), senderWallet.ID.Hex(), senderWallet.Balance, money.Amount)
		return nil, fmt.Errorf("insufficient balance")
	}
	// get receiver wallet
	receiverWallet, err := GetWalletByCurrencySymbol(money.Currency.Symbol, receiverID)
	if err != nil {
		return nil, err
	}
	// create transaction
	trx := NewTransaction(senderID, senderWallet.ID, receiverWallet.ID, money, note)
	// save transaction
	trx.Step = TransactionStepInitiated
	errs := Save(&trx)
	if len(errs) > 0 {
		trx.Step = TransactionStepSenderFailed
		return &trx, JoinErrors(errs)
	}
	// if transaction is saved do the actual transfer
	err = senderWallet.Charge(money)
	if err != nil {
		trx.Step = TransactionStepSenderFailed
		return &trx, err
	}
	err = receiverWallet.Pay(money)
	if err != nil {
		trx.Step = TransactionStepReceiverFailed
		return &trx, err
	}
	trx.Step = TransactionStepAllSuccess
	errs = Update(&trx)
	if len(errs) > 0 {
		return &trx, JoinErrors(errs)
	}
	trx.InformAll()
	return &trx, nil
}

// InformSender sends email to transaction sender
func (t Transaction) InformSender() error {
	status := fmt.Sprintf("%d", t.Step)
	switch t.Step {
	case TransactionStepInitiated:
		status = "not sent"
	case TransactionStepSenderFailed, TransactionStepReceiverFailed:
		status = "failed"
	case TransactionStepSenderSuccess, TransactionStepReceiverSuccess, TransactionStepAllSuccess:
		status = "success"
	}
	subject := fmt.Sprintf("Transaction %s - %s%.2f sent", t.ID.Hex(), t.Amount.Currency.Symbol, t.Amount.Amount)
	msg := fmt.Sprintf("You have sent %s %.2f from your wallet '%s' to wallet ID '%s'.", t.Amount.Currency.Symbol, t.Amount.Amount, t.SenderWallet.Hex(), t.ReceiverWallet.Hex())
	sender, err := GetUserByWalletID(t.SenderWallet)
	if err != nil {
		return err
	}
	receiver, err := GetUserByWalletID(t.ReceiverWallet)
	if err != nil {
		return err
	}
	if !utils.IsValidEmail(sender.Email) {
		return fmt.Errorf("sender email is not valid")
	}
	response := notifications.NewMailResponse(
		subject,
		msg,
		fmt.Sprintf("%s %.2f", t.Amount.Currency.Symbol, t.Amount.Amount),
		"Thanks for using our service.",
		map[string]string{
			"Sender":             fmt.Sprintf("%s %s", sender.Profile.FirstName, sender.Profile.LastName),
			"Receiver":           fmt.Sprintf("%s %s", receiver.Profile.FirstName, receiver.Profile.LastName),
			"Transaction status": status,
		},
		notifications.MsgTypeSentPayment,
	)
	email := notifications.NewEmail(subject, notifications.NoReplyEmail, response, sender.Email)
	resp := email.Send()

	if resp.Error != nil {
		fmt.Printf("EMAIL ERROR: %v\n", resp.Error)
	}
	fmt.Printf("InformSender:\t%s, %s\n", resp.ID, resp.Msg)
	return nil
}

// InformReceiver sends email to transaction receiver
func (t Transaction) InformReceiver() error {
	status := fmt.Sprintf("%d", t.Step)
	switch t.Step {
	case TransactionStepInitiated:
		status = "not sent"
	case TransactionStepSenderFailed, TransactionStepReceiverFailed:
		status = "failed"
	case TransactionStepSenderSuccess, TransactionStepReceiverSuccess, TransactionStepAllSuccess:
		status = "success"
	}
	subject := fmt.Sprintf("Transaction %s - %s%.2f Received", t.ID.Hex(), t.Amount.Currency.Symbol, t.Amount.Amount)
	msg := fmt.Sprintf("You have received %s %.2f in your wallet '%s' from wallet '%s'.", t.Amount.Currency.Symbol, t.Amount.Amount, t.ReceiverWallet.Hex(), t.SenderWallet.Hex())
	receiver, err := GetUserByWalletID(t.ReceiverWallet)
	if err != nil {
		return err
	}
	sender, err := GetUserByWalletID(t.SenderWallet)
	if err != nil {
		return err
	}
	if !utils.IsValidEmail(receiver.Email) {
		return fmt.Errorf("receiver email is not valid")
	}

	response := notifications.NewMailResponse(
		subject,
		msg,
		fmt.Sprintf("%s %.2f", t.Amount.Currency.Symbol, t.Amount.Amount),
		"Thanks for using our service.",
		map[string]string{
			"Sender":             fmt.Sprintf("%s %s", sender.Profile.FirstName, sender.Profile.LastName),
			"Receiver":           fmt.Sprintf("%s %s", receiver.Profile.FirstName, receiver.Profile.LastName),
			"Transaction status": status,
		},
		notifications.MsgTypeReceivedPayment,
	)
	email := notifications.NewEmail(subject, notifications.NoReplyEmail, response, receiver.Email)
	resp := email.Send()
	if resp.Error != nil {
		fmt.Printf("EMAIL ERROR: %v\n", resp.Error)
	}
	fmt.Printf("InformSender:\t%s, %s\n", resp.ID, resp.Msg)
	return nil
}

// InformAll sends notification to both sender and receiver
func (t Transaction) InformAll() {
	go t.InformSender()
	go t.InformReceiver()
}
