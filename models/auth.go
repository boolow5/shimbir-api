package models

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	// UserGenderFemale represents women
	UserGenderFemale = "female"
	// UserGenderMale represents men
	UserGenderMale = "male"
)

// User is user's authentication info
type User struct {
	ID       bson.ObjectId `json:"id" bson:"_id"`
	Email    string        `json:"email" bson:"email"`
	Phone    string        `json:"phone" bson:"phone"`
	Password string        `json:"password" bson:"password"`
	Level    int           `json:"level" bson:"level"`
	Profile  Profile       `json:"profile" bson:"profile"`

	ReviewsID       bson.ObjectId   `json:"reviews_id" bson:"reviews_id,omitempty"`
	ActiveVehicleID bson.ObjectId   `json:"" bson:"active_vehicle_id,omitempty"`
	Wallets         []bson.ObjectId `json:"wallets" bson:"wallets"`
	LicenseNumber   string          `json:"licence_number" bson:"licence_number"`

	Timestamp `bson:",inline"`
}

// Profile is user's personal info
type Profile struct {
	UserID     bson.ObjectId `json:"id" bson:"_id"`
	FirstName  string        `json:"first_name" bson:"first_name"`
	MiddleName string        `json:"middle_name" bson:"middle_name"`
	LastName   string        `json:"last_name" bson:"last_name"`
	Gender     string        `json:"gender" bson:"gender"`
	Birthday   time.Time     `json:"birthday" bson:"birthday"`
	ProfilePic string        `json:"profile_pic" bson:"profile_pic"`
}

// ToMap changes Profile to map
func (p Profile) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"user_id":     p.UserID.Hex(),
		"first_name":  p.FirstName,
		"last_name":   p.LastName,
		"middle_name": p.MiddleName,
		"gender":      p.Gender,
		"birthday":    p.Birthday,
		"profile_pic": p.ProfilePic,
	}
}

// GenerateURL produces profile url for generating QR code etc.
func (p Profile) GenerateURL(domain string) string {
	return fmt.Sprintf("%s/qrcode?userID=%s&itemID=%s&qrType=%s", domain, p.UserID.Hex(), p.UserID.Hex(), QRTypeUser)
}

// GetQRCodeFileName gets profile QRCode file name
func (p Profile) GetQRCodeFileName() string {
	fileName := fmt.Sprintf("%s-%s.png", QRTypeUser, p.UserID.Hex())
	return filepath.Join(config.GetStaticDirectory(), "qrcode", fileName)
}

func (p Profile) FullName() string {
	return fmt.Sprintf("%s %s %s", p.FirstName, p.MiddleName, p.LastName)
}

// GetUserByEmail finds a user by his/her email
func GetUserByEmail(email string) (user User, err error) {
	logger.Debugf("GetUserByEmail(email: %s)", email)
	err = db.FindOne(UserCollection, &bson.M{"email": email}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}

// GetUserByPhone finds a user by his/her phone
func GetUserByPhone(phone string) (user User, err error) {
	logger.Debugf("GetUserByPhone(phone: %s)", phone)
	err = db.FindOne(UserCollection, &bson.M{"phone": phone}, &user)
	if err != nil {
		return user, err
	}
	return user, nil
}

// GetWallets returns all wallets belonging to this user
func (u *User) GetWallets() ([]Wallet, error) {
	// was already created user
	alreadyCreatedUser := len(u.ID.Hex()) == 24
	wallets, err := GetWalletsByUserID(u.ID)
	if err != nil {
		return wallets, err
	}
	if len(wallets) == 0 && alreadyCreatedUser {
		// create new one
		wallet := NewWallet(u.ID, AvailabeCurrencies[0])
		errs := Save(&wallet)
		if len(errs) == 0 {
			err = JoinErrors(errs)
			wallets = append(wallets, wallet)
		}
	}
	if err != nil {
		return wallets, err
	}
	for _, wallet := range wallets {
		if len(wallet.ID.Hex()) == 24 {
			u.Wallets = append(u.Wallets, wallet.ID)
		}
	}
	if alreadyCreatedUser {
		errs := Update(u)
		if len(errs) > 0 {
			return wallets, JoinErrors(errs)
		}
	}
	return wallets, nil
}

// GetColName satisfies DBObject interface
func (u User) GetColName() string {
	return UserCollection
}

// GetID satisfies DBObject interface
func (u User) GetID() bson.ObjectId {
	return u.ID
}

// SetID satisfies DBObject interface
func (u *User) SetID(id bson.ObjectId) {
	u.ID = id
	u.Profile.UserID = id
	fmt.Printf("SetID id=%s, profile %s\n", u.ID.Hex(), u.Profile.UserID.Hex())
	if len(u.Password) < 50 {
		u.SetPassword(u.Password)
	}
}

// SetTimestamp satisfies DBObject interface
func (u *User) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		u.CreatedAt = now
		u.UpdatedAt = now
	} else {
		u.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (u User) IsValid() (valid bool, errs []error) {
	emailIsValid := utils.IsValidEmail(u.Email)
	phoneIsValid := utils.IsValidPhone(u.Phone)
	if !emailIsValid && !phoneIsValid {
		if !emailIsValid {
			errs = append(errs, []error{fmt.Errorf("email")}...)
		} else if !phoneIsValid {
			errs = append(errs, []error{fmt.Errorf("phone")}...)
		}
	}
	if len(u.Profile.UserID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("profile user id")}...)
	}
	if u.Profile.FirstName == "" {
		errs = append(errs, []error{fmt.Errorf("first name")}...)
	}
	return true, errs
}

// SetPassword sets and hashes password of a user object
func (u *User) SetPassword(newPassword string) (err error) {
	newPassword, err = utils.BcryptHash(newPassword)
	if err == nil {
		u.Password = newPassword
		return nil
	}
	return NewError(HashingError, err.Error())
}

// GetToken creates JWT Token
func (u *User) GetToken() (string, error) {
	if len(u.ID.Hex()) != 24 {
		return "", NewError(ValidationError, "invalid user id")
	}
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims["id"] = u.ID.Hex()

	tokenStr, err := token.SignedString([]byte(config.Get().JWTKey))
	return tokenStr, err
}

// Authenticate checks if current user matches the hashed password stored in the database
func (u User) Authenticate(pass string) (bool, error) {
	fmt.Printf("Authenticate(pass: %s) PASS: %s", pass, u.Password)
	if !utils.IsValidEmail(u.Email) && !utils.IsValidPhone(u.Phone) {
		return false, NewError(AuthenticationError, "Invalid email or phone")
	}
	authenticated := utils.AuthenticatePassword(u.Password, pass)
	fmt.Println("authenticated:", authenticated)
	if authenticated {
		return true, nil
	}
	if len(u.Phone) == 0 && len(u.Email) == 0 {
		return false, NewError(AuthenticationError, "Email/phone and password do not match")
	} else if len(u.Email) == 0 {
		return false, NewError(AuthenticationError, "Email and password do not match")
	}
	return false, NewError(AuthenticationError, "Phone and password do not match")
}

// InitRatings initializes and creates reviews/rating tracker
func (u *User) InitRatings() error {
	ratings := UserRating{
		UserID: u.ID,
	}
	errs := Save(&ratings)
	if errs != nil && len(errs) > 0 {
		errMessages := []string{}
		for _, err := range errs {
			errMessages = append(errMessages, string(err.Error()))
		}
		return errors.New(strings.Join(errMessages, ", "))
	}
	return nil
}

// GetWallet gets user by wallet currency symbol
func (u User) GetWallet(currencySymbol string) (wallet Wallet, err error) {
	wallets, err := u.GetWallets()
	if err != nil {
		return wallet, err
	}
	for i := 0; i < len(wallets); i++ {
		if wallets[i].Currency.Symbol == currencySymbol {
			return wallets[i], nil
		}
	}
	return wallet, fmt.Errorf("wallet with this currency not found")
}

// GetVehicles finds vehicles by this user id
func (u User) GetVehicles(own, drive bool) (vehicles []Vehicle, err error) {
	if own && drive {
		return GetVehiclesByUser(u.ID)
	} else if own {
		return GetVehiclesByOwner(u.ID)
	} else if drive {
		return GetVehiclesByDriver(u.ID)
	}
	return vehicles, fmt.Errorf("no vehicle filter, no results")
}
