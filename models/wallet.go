package models

import (
	"fmt"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"gopkg.in/mgo.v2/bson"
)

var (
	holdingWallets []Wallet
	adminWallets   []Wallet
)

const (
	// HodlingWallet name
	HodlingWallet = "holding"
	// AdminWallet name
	AdminWallet = "admin"
	// NormalWallet name
	NormalWallet = ""
)

// Wallet represents a money store that tracks balance left for a user
type Wallet struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name     string        `json:"name" bson:"name"`
	UserID   bson.ObjectId `json:"user_id" bson:"user_id"`
	Currency Currency      `json:"currency" bson:"currency"`
	Balance  float64       `json:"balance" bson:"balance"`

	Timestamp `bson:",inline"`
}

// ToMap converts wallet data to map
func (w Wallet) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"id":       w.ID,
		"name":     w.Name,
		"user_id":  w.UserID,
		"currency": w.Currency,
		"balance":  w.Balance,
	}
}

// NewWallet initializes new wallet
func NewWallet(userID bson.ObjectId, currency Currency) Wallet {
	return Wallet{
		UserID:   userID,
		Currency: currency,
	}
}

// GetColName satisfies DBObject interface
func (w Wallet) GetColName() string {
	return WalletCollection
}

// GetID satisfies DBObject interface
func (w Wallet) GetID() bson.ObjectId {
	return w.ID
}

// SetID satisfies DBObject interface
func (w *Wallet) SetID(id bson.ObjectId) {
	w.ID = id
}

// SetTimestamp satisfies DBObject interface
func (w *Wallet) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		w.CreatedAt = now
		w.UpdatedAt = now
	} else {
		w.UpdatedAt = now
	}
}

// InitWallets initializes all wallets used by the system
func InitWallets(adminID bson.ObjectId) error {
	wallets, err := GetWalletsByUserID(adminID)
	fmt.Printf("________________________\nInitWallets wallets found %d, \nERROR: %v\n", len(wallets), err)
	if err != nil && !db.IsNotFound(err) {
		return err
	} else if db.IsNotFound(err) || len(wallets) == 0 {
		// initialize new wallets
		for _, currency := range AvailabeCurrencies {
			holding := NewWallet(adminID, currency)
			holding.Name = HodlingWallet
			errs := Save(&holding)
			if len(errs) == 0 {
				holdingWallets = append(holdingWallets, holding)
			} else {
				return JoinErrors(errs)
			}
			adWallet := NewWallet(adminID, currency)
			adWallet.Name = AdminWallet
			errs = Save(&adWallet)
			if len(errs) == 0 {
				adminWallets = append(adminWallets, adWallet)
			} else {
				return JoinErrors(errs)
			}
		}
	} else if err == nil {
		for _, wallet := range wallets {
			if wallet.Name == HodlingWallet {
				holdingWallets = append(holdingWallets, wallet)
			} else if wallet.Name == AdminWallet {
				adminWallets = append(adminWallets, wallet)
			} else {
				return fmt.Errorf("Unknown wallet. ID: %s", wallet.ID.Hex())
			}
		}
	}
	return nil
}

// IsValid satisfies DBObject interface
func (w Wallet) IsValid() (valid bool, errs []error) {
	if len(w.UserID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid wallet user id")}...)
	}
	if len(w.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid currency id")}...)
	}
	return true, errs
}

// Charge takes out the amount if the currency is a match
func (w *Wallet) Charge(money Money) error {
	if money.Currency.Symbol != w.Currency.Symbol {
		return fmt.Errorf("charge currency and wallet currecy are different")
	}
	w.Balance -= money.Amount
	errs := Update(w)
	if len(errs) > 0 {
		return JoinErrors(errs)
	}
	return nil
}

// Pay adds the amount if the currency is a match
func (w *Wallet) Pay(money Money) error {
	if money.Currency.Symbol != w.Currency.Symbol {
		return fmt.Errorf("charge currency and wallet currecy are different")
	}
	w.Balance += money.Amount
	errs := Update(w)
	if len(errs) > 0 {
		return JoinErrors(errs)
	}
	return nil
}

// GenerateURL produces wallet url for generating QR code etc.
func (w Wallet) GenerateURL(domain string) string {
	return fmt.Sprintf("%s/qrcode?userID=%s&itemID=%s&qrType=%s", domain, w.UserID.Hex(), w.ID.Hex(), QRTypeWallet)
}

// GetQRCodeFileName gets wallet QRCode file name
func (w Wallet) GetQRCodeFileName() string {
	fileName := fmt.Sprintf("%s-%s.png", QRTypeWallet, w.ID.Hex())
	return filepath.Join(config.GetStaticDirectory(), "qrcode", fileName)
}

// GetWalletsByUserID finds user wallets by his/her id
func GetWalletsByUserID(userID bson.ObjectId) (wallets []Wallet, err error) {
	err = db.FindAll(WalletCollection, &bson.M{"user_id": userID}, &wallets)
	if err != nil {
		return wallets, err
	}
	return wallets, nil
}

// GetUserByWalletID finds user by his/her wallet id
func GetUserByWalletID(walletID bson.ObjectId) (user User, err error) {
	var wallet Wallet
	err = db.FindOne(WalletCollection, &bson.M{"_id": walletID}, &wallet)
	if err != nil {
		return user, err
	}
	err = db.FindOne(UserCollection, &bson.M{"_id": wallet.UserID}, &user)
	return user, nil
}

// GetWalletByCurrencySymbol finds user wallets and returns the one with the currency symbol
func GetWalletByCurrencySymbol(symbol string, userID bson.ObjectId) (wallet Wallet, err error) {
	if len(symbol) < 1 {
		return wallet, fmt.Errorf("currency symbol is empty")
	}
	userWallets, err := GetWalletsByUserID(userID)
	if err != nil {
		return wallet, err
	}
	if len(userWallets) == 0 {
		return wallet, fmt.Errorf("user has no any wallet")
	}
	for i := 0; i < len(userWallets); i++ {
		if userWallets[i].Currency.Symbol == symbol {
			return userWallets[i], nil
		}
	}
	return userWallets[0], fmt.Errorf("wallet with '%s' currency not found", symbol)
}

// GetWalletByCurrencyCode finds user wallets and returns the one with the currency code
func GetWalletByCurrencyCode(code string, userID bson.ObjectId) (wallet Wallet, err error) {
	if len(code) < 1 {
		return wallet, fmt.Errorf("currency code is empty")
	}
	userWallets, err := GetWalletsByUserID(userID)
	if err != nil {
		return wallet, err
	}
	if len(userWallets) == 0 {
		return wallet, fmt.Errorf("user has no any wallet")
	}
	for i := 0; i < len(userWallets); i++ {
		if userWallets[i].Currency.Code == code {
			return userWallets[i], nil
		}
	}
	return userWallets[0], fmt.Errorf("wallet with '%s' currency not found", code)
}

// GetWalletByCurrencyName finds user wallets and returns the one with the currency name
func GetWalletByCurrencyName(name string, userID bson.ObjectId) (wallet Wallet, err error) {
	if len(name) < 1 {
		return wallet, fmt.Errorf("currency name is empty")
	}
	userWallets, err := GetWalletsByUserID(userID)
	if err != nil {
		return wallet, err
	}
	if len(userWallets) == 0 {
		return wallet, fmt.Errorf("user has no any wallet")
	}
	for i := 0; i < len(userWallets); i++ {
		if userWallets[i].Currency.Name == name {
			return userWallets[i], nil
		}
	}
	return userWallets[0], fmt.Errorf("wallet with '%s' currency not found", name)
}

// GetHodlingWallet finds holding wallet by currency symbol
func GetHodlingWallet(currencySymbol string) (wallet Wallet, err error) {
	if len(holdingWallets) == 0 {
		return wallet, fmt.Errorf("no holding wallets found")
	}
	for i := 0; i < len(holdingWallets); i++ {
		if holdingWallets[i].Currency.Symbol == currencySymbol {
			return holdingWallets[i], nil
		}
	}
	return holdingWallets[0], fmt.Errorf("hodling wallet with this currency was not found")
}
