package models

import (
	"fmt"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"
	"gopkg.in/mgo.v2/bson"
)

var (
	// AvailabeCurrencies is all currencies this system supports
	AvailabeCurrencies []Currency
)

// Money represents an amount of money whith it's currency data
type Money struct {
	Amount   float64  `json:"amount" bson:"amount"`
	Currency Currency `json:"currency" bson:"currency"`
}

// Currency represents a currency such as USD, SOS and etc.
type Currency struct {
	ID     bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name   string        `json:"name" bson:"name"`
	Symbol string        `json:"symbol" bson:"symbol"`
	Code   string        `json:"code" bson:"code"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (c Currency) GetColName() string {
	return CurrencyCollection
}

// GetID satisfies DBObject interface
func (c Currency) GetID() bson.ObjectId {
	return c.ID
}

// SetID satisfies DBObject interface
func (c *Currency) SetID(id bson.ObjectId) {
	c.ID = id
}

// SetTimestamp satisfies DBObject interface
func (c *Currency) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		c.CreatedAt = now
		c.UpdatedAt = now
	} else {
		c.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (c Currency) IsValid() (valid bool, errs []error) {
	if len(c.Name) == 0 {
		errs = append(errs, []error{fmt.Errorf("currency name is empty")}...)
	}
	if len(c.Symbol) == 0 {
		errs = append(errs, []error{fmt.Errorf("currency symbol is empty")}...)
	}
	if c.Code == "" {
		errs = append(errs, []error{fmt.Errorf("currency code is empty")}...)
	}
	if len(c.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid currency id")}...)
	}
	return true, errs
}

// GetCurrencyBySymbol finds currency by it's symbol
func GetCurrencyBySymbol(symbol string) (Currency, error) {
	fmt.Printf("GetCurrencyBySymbol(symbol: %s)\n", symbol)
	for i := 0; i < len(AvailabeCurrencies); i++ {
		fmt.Printf("currenc %d = %+v\n", i, AvailabeCurrencies[i])
		if AvailabeCurrencies[i].Symbol == symbol {
			return AvailabeCurrencies[i], nil
		}
	}
	return Currency{}, fmt.Errorf("currency not found")
}

// LoadCurrencies loads available currencies from json file
func LoadCurrencies() error {
	path := filepath.Join(config.GetPath(), "data", "currencies.json")
	return utils.LoadJSONFile(path, &AvailabeCurrencies)
}
