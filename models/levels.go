package models

const (
	// GuestUserLevel means user never used service
	GuestUserLevel = iota
	// NormalUserLevel means user added valid phone number
	NormalUserLevel
	// FirstUserLevel means user completed first ride
	FirstUserLevel
	// SecondUserLevel means user has 3/5 stars
	SecondUserLevel
	// ThirdUserLevel means user has 4/5 stars
	ThirdUserLevel
	// FourthUserLevel means user has 5/5 stars
	FourthUserLevel
	// DriverUserLevel means user was registered as driver with verivied ID
	DriverUserLevel
	// AgentUserLevel means user was authorized to verify drivers identity
	AgentUserLevel
	// ManagerUserLevel means user has access to most of the admin level except the finincial
	ManagerUserLevel
	// AdminUserLevel means user has full access to the service
	AdminUserLevel
)
