package models

import (
	"fmt"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"
	"gopkg.in/mgo.v2/bson"
)

var (
	// VehicleTypes available types of vehicles
	VehicleTypes []VehicleType
)

// Vehicle represents a car, truck, motorcycle or tuktuk
type Vehicle struct {
	ID               bson.ObjectId `json:"id" bson:"_id"`
	Type             string        `json:"type" bson:"type"`
	Model            string        `json:"model" bson:"model"`
	Manufacturer     string        `json:"manufacturer" bson:"manufacturer"`
	PictureURL       string        `json:"picture_url" bson:"picture_url"`
	Seats            int           `json:"seats" bson:"seats"`
	NumberPlate      string        `json:"number" bson:"number"`
	HasAC            bool          `json:"has_ac" bson:"has_ac"`
	DisabilityAccess bool          `json:"disability_access" bson:"disability_access"`

	OwnerID bson.ObjectId   `json:"owner_id" bson:"owner_id,omitempty"`
	Drivers []bson.ObjectId `json:"drivers" bson:"drivers"`

	Timestamp `bson:",inline"`
}

// ToMap converts vehicle to map
func (v Vehicle) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"id":                v.ID,
		"type":              v.Type,
		"model":             v.Model,
		"manufacturer":      v.Manufacturer,
		"picture_url":       v.PictureURL,
		"seats":             v.Seats,
		"number":            v.NumberPlate,
		"has_ac":            v.HasAC,
		"disability_access": v.DisabilityAccess,
		"owner_id":          v.OwnerID,
		"drivers":           v.Drivers,
	}
}

// NewVehicle initializes vehicle struct
func NewVehicle(vType, model, manufacturer, numberPlate, picURL string, seatCount int, hasAC, disabilityAccess bool, ownerID bson.ObjectId, drivers []bson.ObjectId) Vehicle {
	return Vehicle{
		Type:             vType,
		Model:            model,
		Manufacturer:     manufacturer,
		PictureURL:       picURL,
		Seats:            seatCount,
		NumberPlate:      numberPlate,
		HasAC:            hasAC,
		DisabilityAccess: disabilityAccess,
		OwnerID:          ownerID,
		Drivers:          drivers,
	}
}

// VehicleType represents a type of vehicle supported by the system
type VehicleType struct {
	Name     string `json:"name" bson:"name"`
	Type     string `json:"type" bson:"type"`
	MinSeats int    `json:"min_seats" bson:"min_seats"`
	MaxSeats int    `json:"max_seats" bson:"max_seats"`
}

// GetColName satisfies DBObject interface
func (v Vehicle) GetColName() string {
	return VehicleCollection
}

// GetID satisfies DBObject interface
func (v Vehicle) GetID() bson.ObjectId {
	return v.ID
}

// SetID satisfies DBObject interface
func (v *Vehicle) SetID(id bson.ObjectId) {
	v.ID = id
}

// SetTimestamp satisfies DBObject interface
func (v *Vehicle) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		v.CreatedAt = now
		v.UpdatedAt = now
	} else {
		v.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (v Vehicle) IsValid() (valid bool, errs []error) {
	if len(v.ID.Hex()) != 24 {
		errs = append(errs, fmt.Errorf("invalid vehicle id"))
	}
	if v.Seats < 2 {
		errs = append(errs, fmt.Errorf("vehicle has no enough seats"))
	}
	// vehicle has owner or driver
	hasNoOwner := len(v.OwnerID.Hex()) != 24
	// vehicle has one ore more drivers
	hasNoDrivers := len(v.Drivers) == 0
	if hasNoDrivers && hasNoOwner {
		errs = append(errs, fmt.Errorf("vehicle has no owner or driver"))
	}
	return true, errs
}

// GenerateURL produces vehicle url for generating QR code etc.
func (v Vehicle) GenerateURL(domain string) string {
	return fmt.Sprintf("%s/qrcode?userID=%s&itemID=%s&qrType=%s", domain, v.OwnerID.Hex(), v.ID.Hex(), QRTypeVehicle)
}

// GetQRCodeFileName gets vehicle QRCode file name
func (v Vehicle) GetQRCodeFileName() string {
	fileName := fmt.Sprintf("%s-%s.png", QRTypeVehicle, v.ID.Hex())
	return filepath.Join(config.GetStaticDirectory(), "qrcode", fileName)
}

// LoadVehicleTypes loads vehicle types from data/vehicles.json file
func LoadVehicleTypes() (int, error) {
	logger.Debugf("loadVehicleTypes")
	fullPath := filepath.Join(config.GetPath(), "data", "vehicles.json")
	err := utils.LoadJSONFile(fullPath, &VehicleTypes)
	if err != nil {
		logger.Errorf("Cannot load %s.\nREASON: %v", fullPath, err)
		return 0, err
	}
	logger.Debugf("Found %d vehicle types", len(VehicleTypes))
	return len(VehicleTypes), nil
}

// IsValidVehicleType checks if the vType is a valid vehicle in the system
func IsValidVehicleType(vType string) bool {
	for i := 0; i < len(VehicleTypes); i++ {
		if VehicleTypes[i].Type == vType {
			return true
		}
	}
	return false
}

// GetVehiclesByOwner finds vehicle by its owner id
func GetVehiclesByOwner(ownerID bson.ObjectId) (vehicles []Vehicle, err error) {
	err = db.FindAll(VehicleCollection, &bson.M{"owner_id": ownerID}, &vehicles)
	return vehicles, err
}

// GetVehiclesByDriver finds vehicle by its driver id
func GetVehiclesByDriver(driverID bson.ObjectId) (vehicles []Vehicle, err error) {
	err = db.FindAll(VehicleCollection, &bson.M{"drivers": driverID}, &vehicles)
	return vehicles, err
}

// GetVehiclesByUser finds vehicle by its owner/driver id
func GetVehiclesByUser(userID bson.ObjectId) (vehicles []Vehicle, err error) {
	err = db.FindAll(VehicleCollection, &bson.M{
		"$or": []bson.M{
			{"owner_id": userID},
			{"drivers": userID},
		},
	}, &vehicles)
	return vehicles, err
}
