package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Driver represents car, motorcycle, or tuktuk driver
type Driver struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserID    bson.ObjectId `json:"user_id" bson:"user_id,omitempty"`
	Verified  bool
	Vehicles  []bson.ObjectId
	ReviewsID bson.ObjectId `json:"reviews_id" bson:"reviews_id,omitempty"`

	Timestamp `bson:",inline"`
}

// Passenger represents the target consumer of this service
type Passenger struct {
	UserID    bson.ObjectId `json:"user_id" bson:"user_id,omitempty"`
	ReviewsID bson.ObjectId `json:"review_id" bson:"review_id,omitempty"`
	FullName  string        `json:"full_name" bson:"full_name"`
}

// NewDriver initializes driver object
func NewDriver(userID, reviewsID bson.ObjectId, verified bool, vehicles []bson.ObjectId) Driver {
	return Driver{
		UserID:    userID,
		Verified:  verified,
		Vehicles:  vehicles,
		ReviewsID: reviewsID,
	}
}

// NewPassenger initializes passenger object
func NewPassenger(userID, reviewID bson.ObjectId, fullName string) Passenger {
	return Passenger{
		UserID:    userID,
		ReviewsID: reviewID,
		FullName:  fullName,
	}
}

// GetColName satisfies DBObject interface
func (d Driver) GetColName() string {
	return DriverCollection
}

// GetID satisfies DBObject interface
func (d Driver) GetID() bson.ObjectId {
	return d.ID
}

// SetID satisfies DBObject interface
func (d *Driver) SetID(id bson.ObjectId) {
	d.ID = id
}

// SetTimestamp satisfies DBObject interface
func (d *Driver) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		d.CreatedAt = now
		d.UpdatedAt = now
	} else {
		d.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (d Driver) IsValid() (valid bool, errs []error) {
	if len(d.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid driver id")}...)
	}
	if len(d.Vehicles) < 1 {
		errs = append(errs, []error{fmt.Errorf("cannot save driver without vehicle")}...)
	}
	if len(d.UserID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid profile id")}...)
	}
	return true, errs
}
