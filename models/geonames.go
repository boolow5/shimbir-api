package models

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"gopkg.in/mgo.v2/bson"
)

// District is administrative section of a city or province
type District struct {
	ID       bson.ObjectId `json:"id" bson:"_id"`
	Name     string        `json:"name" bson:"name"`
	AltNames []string      `json:"alt_names" bson:"alt_names"`

	Center    Location           `json:"center" bson:"center"`
	Northern  Location           `json:"northern" bson:"northern"`
	Eastern   Location           `json:"eastern" bson:"eastern"`
	Southern  Location           `json:"southern" bson:"southern"`
	Western   Location           `json:"western" bson:"western"`
	Neighbors []NeighborDistrict `json:"neighbors" bson:"neighbors"`

	Timestamp `bson:",inline"`
}

// NeighborDistrict indicates close district and which direction
type NeighborDistrict struct {
	Direction string        `json:"direction" bson:"direction"`
	District  bson.ObjectId `json:"district" bson:"district,omitempty"`
}

// GetColName satisfies DBObject interface
func (d District) GetColName() string {
	return DistrictCollection
}

// GetID satisfies DBObject interface
func (d District) GetID() bson.ObjectId {
	return d.ID
}

// SetID satisfies DBObject interface
func (d *District) SetID(id bson.ObjectId) {
	d.ID = id
}

// SetTimestamp satisfies DBObject interface
func (d *District) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		d.CreatedAt = now
		d.UpdatedAt = now
	} else {
		d.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (d District) IsValid() (valid bool, errs []error) {
	if len(d.Name) < 1 {
		logger.Error("district.Name is empty")
		errs = append(errs, []error{fmt.Errorf("city")}...)
	}
	if valid, es := d.Northern.IsValid(); !valid || len(es) > 0 {
		logger.Error("district.Norther ", es)
		errs = append(errs, []error{fmt.Errorf("invalid north")}...)
	}
	if valid, es := d.Eastern.IsValid(); !valid || len(es) > 0 {
		logger.Error("district.Eastern ", es)
		errs = append(errs, []error{fmt.Errorf("invalid eastern")}...)
	}
	if valid, es := d.Southern.IsValid(); !valid || len(es) > 0 {
		logger.Error("district.Southern ", es)
		errs = append(errs, []error{fmt.Errorf("invalid southern")}...)
	}
	if valid, es := d.Western.IsValid(); !valid || len(es) > 0 {
		logger.Error("district.Western ", es)
		errs = append(errs, []error{fmt.Errorf("invalid western")}...)
	}
	if valid, es := d.Center.IsValid(); !valid || len(es) > 0 {
		logger.Error("district.Center ", es)
		errs = append(errs, []error{fmt.Errorf("invalid center")}...)
	}
	// // check all neighbors have valid ids
	// for i := 0; i < len(d.Neighbors); i++ {
	// 	if d.Neighbors[i].Direction != "" {
	// 		errs = append(errs, []error{fmt.Errorf("invalid neighbor direction")}...)
	// 	}
	// 	if len(d.Neighbors[i].District.ID.Hex()) != 24 {
	// 		errs = append(errs, []error{fmt.Errorf("invalid neighbor district id")}...)
	// 	}
	// }
	if len(errs) == 0 || errs == nil {
		return true, nil
	}
	return false, errs
}

// LoadDistrictBoundaries laods district boundaries from data/districts.csv
func LoadDistrictBoundaries() error {
	file, err := os.Open(filepath.Join(config.GetPath(), "data", "districts.csv"))
	if err != nil {
		return err
	}
	newDistricts := []District{}
	districtLocation := Location{}
	districtLocation.Address.City = "Mogadishu"
	districtLocation.Address.Country = "Somalia"
	districtLocation.Address.CountryCode = "SO"
	districtLocation.Address.State = "Banaadir"
	firstLine := []string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if len(firstLine) == 0 {
			firstLine = strings.Split(line, ",")
			continue
		}
		parts := strings.Split(line, ",")
		if len(parts) < 12 {
			for i, part := range parts {
				fmt.Println(i+1, part)
			}
			return fmt.Errorf("expected 12 columns but got %d", len(parts))
		}
		var district District
		district.Name = parts[0]

		districtLocation.Address.FormattedAddress = fmt.Sprintf("%s, %s, %s, %s", district.Name, districtLocation.Address.City, districtLocation.Address.State, districtLocation.Address.Country)

		district.AltNames = strings.Split(parts[1], ";")

		district.Center = districtLocation
		district.Center.Lat, _ = strconv.ParseFloat(parts[2], 64)
		district.Center.Long, _ = strconv.ParseFloat(parts[3], 64)

		district.Northern = districtLocation
		district.Northern.Lat, _ = strconv.ParseFloat(parts[4], 64)
		district.Northern.Long, _ = strconv.ParseFloat(parts[5], 64)

		district.Eastern = districtLocation
		district.Eastern.Lat, _ = strconv.ParseFloat(parts[6], 64)
		district.Eastern.Long, _ = strconv.ParseFloat(parts[7], 64)

		district.Southern = districtLocation
		district.Southern.Lat, _ = strconv.ParseFloat(parts[8], 64)
		district.Southern.Long, _ = strconv.ParseFloat(parts[9], 64)

		district.Western = districtLocation
		district.Western.Lat, _ = strconv.ParseFloat(parts[10], 64)
		district.Western.Long, _ = strconv.ParseFloat(parts[11], 64)

		newDistricts = append(newDistricts, district)
	}
	for _, district := range newDistricts {
		fmt.Printf("\n%s:\n", district.Name)
		errs := Save(&district)
		if errs != nil {
			fmt.Printf("cannot save %s district because of the following:\n", district.Name)
			for i, err := range errs {
				fmt.Printf("ERROR [%d] %v\n", i, err)
			}
		}
	}
	return nil
}

// GetNearbyDistricts finds districts near this coordinates within the maxRadius in kilometers
func GetNearbyDistricts(lat, long, maxRadius float64) ([]District, error) {
	query := bson.M{"center": bson.M{
		"$near": bson.M{
			"$geometry": bson.M{
				"type":        "Point",
				"coordinates": []float64{long, lat},
			},
			"$maxDistance": maxRadius * 1000,
		}},
	}
	var results []District
	err := db.FindAll(DistrictCollection, &query, &results)
	if err != nil {
		return results, err
	}
	return results, nil
}
