package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/utils"
	geo "github.com/codingsince1985/geo-golang"
	"gopkg.in/mgo.v2/bson"
)

// Location represents a point on earth
type Location struct {
	ID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Long    float64       `json:"long" bson:"long"`
	Lat     float64       `json:"lat" bson:"lat"`
	Backend string        `json:"backed" bson:"backed"`
	Address geo.Address   `json:"address" bson:"address"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (l Location) GetColName() string {
	return LocationCollection
}

// GetID satisfies DBObject interface
func (l Location) GetID() bson.ObjectId {
	return l.ID
}

// SetID satisfies DBObject interface
func (l *Location) SetID(id bson.ObjectId) {
	l.ID = id
}

// SetTimestamp satisfies DBObject interface
func (l *Location) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		l.CreatedAt = now
		l.UpdatedAt = now
	} else {
		l.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (l Location) IsValid() (valid bool, errs []error) {
	if len(l.Address.City) < 1 {
		errs = append(errs, []error{fmt.Errorf("city")}...)
	}
	if len(l.Address.CountryCode) != 2 {
		errs = append(errs, []error{fmt.Errorf("country_code")}...)
	}
	if l.Lat == 0 {
		errs = append(errs, []error{fmt.Errorf("latitude")}...)
	}
	if l.Long == 0 {
		errs = append(errs, []error{fmt.Errorf("longitude")}...)
	}
	return true, errs
}

// Geocode sets location coordinates by using the address data
func (l *Location) Geocode() error {
	if len(l.Address.FormattedAddress) == 0 {
		return NewError(InvalidLocationAddress, "formatted address is empty")
	}
	coords, backend, err := utils.GeoCode(l.Address.FormattedAddress)
	if err != nil {
		return err
	}
	l.Lat = coords.Lat
	l.Long = coords.Lng
	l.Backend = backend
	return nil
}

// ReverseGeocode sets location address by using the coordinates data
func (l *Location) ReverseGeocode() error {
	if l.Lat == 0 && l.Long == 0 {
		return NewError(InvalidLocationCoordinates, "coordinates are both zero")
	}
	address, backend, err := utils.ReverseGeocode(l.Lat, l.Long)
	if err != nil {
		return err
	}
	l.Address = *address
	l.Backend = backend
	return nil
}

// GetDistance calculates distance (meters) between two points on earth
func GetDistance(from, to Location) float64 {
	return utils.Distance(from.Lat, from.Long, to.Lat, to.Long) // utils.DistanceInKm(from.Lat, from.Long, to.Lat, to.Long) * 1000
}
