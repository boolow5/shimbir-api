package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// UserRating represents all reviews about a user
type UserRating struct {
	ID             bson.ObjectId   `json:"id" bson:"_id"`
	UserID         bson.ObjectId   `json:"user_id" bson:"user_id"`
	Reviews        []bson.ObjectId `json:"reviews" bson:"reviews"`
	FiveStarCount  int             `json:"five_star_count" bson:"five_star_count"`
	FourStarCount  int             `json:"four_star_count" bson:"four_star_count"`
	ThreeStarCount int             `json:"three_star_count" bson:"three_star_count"`
	TwoStarCount   int             `json:"two_star_count" bson:"two_star_count"`
	OneStarCount   int             `json:"one_star_count" bson:"one_star_count"`
	AverageRate    float32         `json:"average_rate" bson:"average_rate"`
	RateCount      int             `json:"rate_count" bson:"rate_count"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (ur UserRating) GetColName() string {
	return UserRatingCollection
}

// GetID satisfies DBObject interface
func (ur UserRating) GetID() bson.ObjectId {
	return ur.ID
}

// SetID satisfies DBObject interface
func (ur *UserRating) SetID(id bson.ObjectId) {
	ur.ID = id
}

// SetTimestamp satisfies DBObject interface
func (ur *UserRating) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		ur.CreatedAt = now
		ur.UpdatedAt = now
	} else {
		ur.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (ur UserRating) IsValid() (valid bool, errs []error) {
	if len(ur.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid rating id")}...)
	}
	if len(ur.UserID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid user id")}...)
	}
	return true, errs
}
