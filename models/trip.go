package models

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/notifications"
	"gopkg.in/mgo.v2/bson"
)

const (
	// TripStatusOrdered user requested for a driver
	TripStatusOrdered = iota
	// TripStatusAccepted driver accepted to drive
	TripStatusAccepted
	// TripStatusEnroute driver and passenger are both on the vehicle
	TripStatusEnroute
	// TripStatusDropoff trip reached destination
	TripStatusDropoff
	// TripStatusCompleted user paid for the trip
	TripStatusCompleted
	// TripStatusCancelled user cancelled before any driver accepted the trip
	TripStatusCancelled
)

// Trip represents travel route from location to another requested by passenger(s) driven by driver
type Trip struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	CreatedBy bson.ObjectId `json:"created_by" bson:"created_by"`

	Start Location `json:"star" bson:"star"`
	End   Location `json:"end" bson:"end"`

	Driver     Driver      `json:"driver" bson:"driver"`
	Passengers []Passenger `json:"passengers" bson:"passengers"`
	Status     int         `json:"status" bson:"status"`

	EstimatedDistance float64 `json:"estimated_distance" bson:"estimated_distance"`
	ServiceAmount     Money   `json:"service_amount" bson:"service_amount"`
	EstimatedPrice    Money   `json:"estimated_price" bson:"estimated_price"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (t Trip) GetColName() string {
	return TripCollection
}

// GetID satisfies DBObject interface
func (t Trip) GetID() bson.ObjectId {
	return t.ID
}

// SetID satisfies DBObject interface
func (t *Trip) SetID(id bson.ObjectId) {
	t.ID = id
}

// SetTimestamp satisfies DBObject interface
func (t *Trip) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		t.CreatedAt = now
		t.UpdatedAt = now
	} else {
		t.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (t Trip) IsValid() (valid bool, errs []error) {
	if len(t.ID.Hex()) != 24 {
		errs = append(errs, []error{fmt.Errorf("invalid trip id")}...)
	}
	if len(t.Passengers) < 1 {
		errs = append(errs, []error{fmt.Errorf("trip has no passenger")}...)
	}
	if t.EstimatedDistance == 0 {
		errs = append(errs, []error{fmt.Errorf("trip has no estimated distance")}...)
	}
	return true, errs
}

// SetDriver sets driver for a trip and notifies if allowed to the passenger
func (t *Trip) SetDriver(driver User, vehicleID bson.ObjectId, responseChannel string, notify bool) error {
	if driver.Level < DriverUserLevel {
		fmt.Println("failed to set driver: permission level too low")
		return fmt.Errorf("permission level too low")
	}
	t.Driver = NewDriver(driver.ID, driver.ReviewsID, driver.Level >= DriverUserLevel, nil)

	msg := notifications.DriverFoundMessage{
		TripID:          t.ID,
		VehicleID:       vehicleID,
		ResponseChannel: responseChannel,
	}
	data, err := json.Marshal(&msg)
	if err != nil {
		fmt.Println("failed to marshal sse message: ", err)
		return err
	}
	errs := Update(t)
	if len(errs) > 0 {
		fmt.Println("failed to to update trip: ", errs)
		return JoinErrors(errs)
	}
	return notifications.Send(data, []string{"sse"}, map[string]interface{}{"channel": msg.ResponseChannel})
}
