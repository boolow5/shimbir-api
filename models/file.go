package models

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"gopkg.in/mgo.v2/bson"
)

const (
	// StaticFilePurposeProfilePic means file is used for user profile, userID can have one profile pic, rest are removed
	StaticFilePurposeProfilePic = "profile_pic"
	// StaticFilePurposeVehiclePic means file is used as vehicle pic, each itemID can one pic, rest are removed
	StaticFilePurposeVehiclePic = "vehicle_pic"
	// StaticFilePurposeDriverPic means file is used for driver profile, it's different from the user profile which can be updated by the user
	// Driver pic can only be updated by agent or user with higher level
	StaticFilePurposeDriverPic = "driver_pic"
)

// StaticFile represents and tracks uploaded files
type StaticFile struct {
	ID   bson.ObjectId `json:"id" bson:"_id"`
	Name string        `json:"name" bson:"name"`
	URL  string        `json:"url" bson:"url"`
	Dir  string        `json:"dir" bson:"dir"`

	UserID  bson.ObjectId `json:"user_id" bson:"user_id"`
	ItemID  bson.ObjectId `json:"item_id" bson:"item_id"`
	UsedFor string        `json:"used_for" bson:"used_for"`

	Timestamp `bson:",inline"`
}

// GetColName satisfies DBObject interface
func (sf StaticFile) GetColName() string {
	return StaticFileCollection
}

// GetID satisfies DBObject interface
func (sf StaticFile) GetID() bson.ObjectId {
	return sf.ID
}

// SetID satisfies DBObject interface
func (sf *StaticFile) SetID(id bson.ObjectId) {
	sf.ID = id
}

// SetTimestamp satisfies DBObject interface
func (sf *StaticFile) SetTimestamp(isCreate bool) {
	now := time.Now()
	if isCreate {
		sf.CreatedAt = now
		sf.UpdatedAt = now
	} else {
		sf.UpdatedAt = now
	}
}

// IsValid satisfies DBObject interface
func (sf StaticFile) IsValid() (valid bool, errs []error) {
	if len(sf.ID.Hex()) != 24 {
		errs = append(errs, fmt.Errorf("invalid file id"))
	}
	if len(strings.TrimSpace(sf.Dir)) == 0 {
		errs = append(errs, fmt.Errorf("invalid file destination"))
	}
	return true, errs
}

// GetStaticDir returns static directory
func GetStaticDir() string {
	return config.Get().StaticDirectory
}

// GetStaticFiles finds files data by user id and item id
func GetStaticFiles(userID, itemID bson.ObjectId) (files []StaticFile, err error) {
	err = db.FindAll(StaticFileCollection, &bson.M{
		"user_id": userID,
		"item_id": itemID,
	}, &files)
	return files, err
}
