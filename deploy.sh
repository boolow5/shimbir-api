#!/bin/bash

export HOST="178.79.191.179"
# Package name
export API_PACKAGE=qaado.tar.gz
# The remote directory of the server
export DEPLOY_PATH=/webapps/qaado
# remote server user
export OWNER=mahdi
# web user that runs the web app
export WEB_USER=qaado
# process name
export PROC_NAME=qaado

scp $API_PACKAGE ${OWNER}@${HOST}:/tmp

echo "sending \"$API_PACKAGE\" to \"$DEPLOY_PATH/api/bin\""

ssh -t ${OWNER}@${HOST} "
  sudo rm -r $DEPLOY_PATH/api/bin &&
  sudo tar -xvzf /tmp/$API_PACKAGE -C $DEPLOY_PATH/api/;
  sudo find $DEPLOY_PATH/api/bin -type d -exec chmod 755 {} \;
  sudo find $DEPLOY_PATH/api/bin -type f -exec chmod 754 {} \;
  sudo chown -R $WEB_USER $DEPLOY_PATH/api/bin;
  sudo supervisorctl restart ${PROC_NAME} || true
";
