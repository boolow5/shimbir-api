package router

import (
	"net/http"
	"path/filepath"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/middlewares"
	"github.com/gin-gonic/gin"
)

// Init initializes API routes, endpionts and middlware integrations
func Init() *gin.Engine {
	router := gin.Default()
	// set maximum file size
	router.MaxMultipartMemory = 1 << 20
	router.Use(middlewares.CORS())
	// router.Static(filepath.Join(config.GetPath(), "static"), "./static")
	router.StaticFS(filepath.Join(config.GetPath(), "static"), http.Dir("static"))
	setupV1(router)
	return router
}
