package router

import (
	"net/http"

	"bitbucket.org/boolow5/carpooling/shimbir-api/controllers"
	"bitbucket.org/boolow5/carpooling/shimbir-api/middlewares"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"github.com/gin-gonic/gin"
)

func setupV1(router *gin.Engine) {

	// routes for everyone, even non-logged in
	v1 := router.Group("/api/v1")
	{
		v1.GET("/version", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"version": models.Version,
				"type":    "v1",
				"build":   models.CommitNo,
				"date":    models.LastBuidDate,
				"domain":  c.Request.Host,
			})
		})
		v1.GET("/ping", controllers.Ping)
		v1.POST("/signup", controllers.Signup)
		v1.POST("/login", controllers.Login)
		v1.POST("/reversegeocode", controllers.ReverseGeocode)
		v1.POST("/geocode", controllers.Geocode)
		v1.POST("/calculate-distance", controllers.CalcuateDistance)
	}
	authorized := v1.Group("/")

	// routes for all logged in users
	authorized.Use(middlewares.Auth())
	{
		authorized.GET("/profile", controllers.GetProfile)
		authorized.POST("/upload", controllers.UploadPicture)
		authorized.POST("/trip", controllers.AddTrip)
		authorized.PUT("/trip", controllers.UpdateTrip)
		authorized.PUT("/trip/cancel", controllers.CancelTrip)
		authorized.POST("/nearby-districts", controllers.GetNearbyDistricts)
		authorized.POST("/notify-nearby-driver", controllers.NotifyDrivers)
		authorized.GET("/is-driver", controllers.GetIsDriver)
		authorized.GET("/user-level", controllers.GetUserLevel)

		authorized.GET("/wallets", controllers.GetWallets)
		authorized.GET("/wallet/:walletID", controllers.GetWallet)
		authorized.POST("/transfer", controllers.TransferAmount)
	}

	// driver specific routes
	driver := authorized.Group("/driver")
	{
		driver.POST("/accept-trip", controllers.AcceptTrip)
		driver.GET("/vehicles", controllers.GetVehicles)
	}

	// agent specific routes
	agent := authorized.Group("/agent")
	{
		agent.POST("/vehicle", controllers.RegisterVehicle)
	}

	// admin specific routes
	admin := authorized.Group("/admin")
	{
		admin.PUT("/reload-vehicle-types", controllers.ReloadVehicleTypes)
		admin.POST("/driver", controllers.AddDriver)
		admin.PUT("/driver", controllers.UpdateDriver)
		admin.PUT("/reload-wallet", controllers.ReloadAdminWallet)
	}
}
