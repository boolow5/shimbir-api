package main

import (
	"fmt"

	"bitbucket.org/boolow5/carpooling/shimbir-api/config"
	"bitbucket.org/boolow5/carpooling/shimbir-api/db"
	"bitbucket.org/boolow5/carpooling/shimbir-api/logger"
	"bitbucket.org/boolow5/carpooling/shimbir-api/models"
	"bitbucket.org/boolow5/carpooling/shimbir-api/notifications"
	"bitbucket.org/boolow5/carpooling/shimbir-api/router"
)

func main() {
	logger.Init(logger.DebugLevel)
	models.Version = config.Get().Version
	fmt.Println("Starting qaado carpooling api...")
	db.Init()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	err = models.LoadDistrictBoundaries()
	if err != nil {
		panic(fmt.Sprintln("LoadDistrictBoundaries ERROR:", err))
	}
	router := router.Init()
	conf := config.Get()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
